﻿using ScrumGame.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace XUnitScrumGameTests
{
    public class DeckTest
    {
        [Fact]
        public void GetCardIndexTest()
        {
            int index = 0;
            Assert.Equal(index, Deck.getCardIndex(CardType.UserStory, index));
            
            index = 1;
            Assert.Equal(index, Deck.getCardIndex(CardType.ProductGoal, index));

            index = -1;
            Assert.NotEqual(index, Deck.getCardIndex(CardType.UserStory, index));
        }

        [Fact]
        public void PullMessageTest()
        {
            /* Checks that every string gets pulled and that they are equal */
            string msg;
            for (int i = 0; i >= 1; i++)
            {
                foreach (var c in Deck.cardsInDeck[i])
                {
                    msg = Deck.pullMessage((CardType)i);
                    if (c == msg)
                        Assert.Contains(c, msg);
                }
                Assert.Empty(Deck.cardsInDeck[i]);
            }
        }
    }
}
