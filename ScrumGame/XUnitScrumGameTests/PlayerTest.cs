﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using ScrumGame.Models;

namespace XUnitScrumGameTests
{
	public class PlayerTest
	{
		[Fact]
		public void TestConstrustor()
		{
			Player p = new Player("Alec H", new DateTime(1997, 11, 16));
			Assert.Equal("Alec H", p.Name);
			//Assert.Equal(new DateTime(1997, 11, 16), p.DoB);
		}

		[Fact]
		public void TestRollDice()
		{
			Player p = new Player("Alec H", new DateTime(1997, 11, 16));
			for (int i = 0; i < 1000; i++)
			{
				int roll = p.RollDice(1)[0];
				Assert.InRange(roll, 1, 6);
			}
		}

		[Fact]
		public void TestGetGoal() {	

			/* Make sure cards have the correct messages */
			Player p = new Player("Austin J", new DateTime(1999, 04, 29));
			p.GetGoal(1);
			Assert.Equal ( "o" , Deck.productGoals[1] );
		}

		[Fact]
		public void TestInitMeeples() {
			/* Makes sure initMeeples generates the correct number of meeples, 
			 * given _INIT_MEEPLES_DEV = 4 */
			int dcount = 0, pcount = 0, scount = 0;
			Player p = new Player("Austin J", new DateTime(1999, 04, 29));
			foreach (var meeple in p.Meeples)
			{
				if (meeple == Meeple.DEVELOPER ) dcount++;
				else if ( meeple == Meeple.PRODUCT_OWNER ) pcount++;
				else if ( meeple == Meeple.SCRUM_MASTER ) scount++;
			}
			Assert.Equal ( 4, dcount );
			Assert.Equal ( 1, scount );
			Assert.Equal ( 1, pcount );
		}

		[Fact]
		public void RemoveMeepleTest()
		{ 
			Player p = new Player("Alec H", new DateTime(1999, 04, 29));
			Meeple dev = Meeple.DEVELOPER;
			p.RemoveMeeple(dev);
			Assert.Equal(5, p.Meeples.Count);
		}

		[Fact]
		public void AddMeepleTest()
		{
			Player p = new Player("Austin J", new DateTime(1999, 04, 29));
			int OldMeepleCount = p.Meeples.Count;
			Meeple dev = Meeple.DEVELOPER;
			
			p.AddMeeple(dev);
			Assert.Equal(OldMeepleCount + 1, p.Meeples.Count);

			Assert.Equal(dev, p.Meeples[p.Meeples.Count - 1]);
		}
	}
}
