using Moq;
using ScrumGame.Models;
using ScrumGame.Controllers;
using System;
using System.Collections.Generic;
using Xunit;

namespace XUnitScrumGameTests
{
	
	public class GameControllerTest
	{

/*		[Fact]
		public void AddPlayerTest()
		{
			GameController g = new GameController();
			Player p = new Player("Austin J", new DateTime(1999, 04, 29));

			g.AddPlayer(p);

			Player Austin = g.Players[1]; //Alec added as p0 in initilization
			Assert.Equal("Austin J", Austin.Name);
			Assert.Equal(new DateTime(1999, 04, 29).ToShortDateString(), Austin.DoB);
		}

		[Fact]
		public void RemovingPlayerTest()
		{
			GameController g = new GameController();
			List<Player> players = new List<Player>(g.Players);
			foreach (var p in players)
			{
				g.RemovePlayer(p);
			}

			Assert.Empty(g.Players);
		}

		[Fact]
		public void WinByUseCasesCheckTest()
		{
			Mock<Player> p = new Mock<Player>("Austin J", new DateTime(1999, 04, 29));
			p.SetupGet(x => x.UseCasesRemaining).Returns(0);
			GameController g = new GameController();
			
			g.AddPlayer(p.Object);
			
			Player winner = g.WinByUseCasesCheck();
			Assert.Equal(p.Object, winner);
		}

		*//*
		[Fact]
		public void DeterminePlayerOrderTest()
		{
			GameController g = new GameController();
			Mock<Player> p1 = new Mock<Player>("Alec H", new DateTime(1997, 1, 16));
			Mock<Player> p2 = new Mock<Player>("Chris P", new DateTime(1989, 5, 5));
			Mock<Player> p3 = new Mock<Player>("Mike H", new DateTime(1921, 9, 9));
			Mock<Player> p4 = new Mock<Player>("Ben D", new DateTime(1999, 12, 21));

			p1.Setup(f => f.RollDice(1)).Returns(new int[1] { 1 });
			p2.Setup(f => f.RollDice(1)).Returns(new int[1] { 2 });
			p3.Setup(f => f.RollDice(1)).Returns(new int[1] { 3 });
			p4.Setup(f => f.RollDice(1)).Returns(new int[1] { 4 });

			Assert.Equal(new int[1] { 1 }, p1.Object.RollDice(1));
			Assert.Equal(new int[1] { 2 }, p2.Object.RollDice(1));
			Assert.Equal(new int[1] { 3 }, p3.Object.RollDice(1));
			Assert.Equal(new int[1] { 4 }, p4.Object.RollDice(1));

			g.AddPlayer(p1.Object);
			g.AddPlayer(p2.Object);
			g.AddPlayer(p3.Object);
			g.AddPlayer(p4.Object);

			List<Player> order = g.DeterminePlayerOrder();

			Assert.Equal(p4.Object, order[3]);
			Assert.Equal(p3.Object, order[2]);
			Assert.Equal(p2.Object, order[1]);
			Assert.Equal(p1.Object, order[0]);

		}*//*

		[Fact]
		public void RollOffCompetition()
        {
			GameController g = new GameController();
			Mock<Player> p1 = new Mock<Player>("Alec H", new DateTime(1997, 1, 16));
			Mock<Player> p2 = new Mock<Player>("Chris P", new DateTime(1989, 5, 5));
			Mock<Player> p3 = new Mock<Player>("Mike H", new DateTime(1921, 9, 9));
			Mock<Player> p4 = new Mock<Player>("Ben D", new DateTime(1999, 12, 21));

			p1.Setup(f => f.RollDice(1)).Returns(new int[1] { 1 });
			p2.Setup(f => f.RollDice(1)).Returns(new int[1] { 2 });
			p3.Setup(f => f.RollDice(1)).Returns(new int[1] { 3 });
			p4.Setup(f => f.RollDice(1)).Returns(new int[1] { 4 });

			Assert.Equal(new int[1] { 1 }, p1.Object.RollDice(1));
			Assert.Equal(new int[1] { 2 }, p2.Object.RollDice(1));
			Assert.Equal(new int[1] { 3 }, p3.Object.RollDice(1));
			Assert.Equal(new int[1] { 4 }, p4.Object.RollDice(1));

			g.AddPlayer(p1.Object);
			g.AddPlayer(p2.Object);
			g.AddPlayer(p3.Object);
			g.AddPlayer(p4.Object);

			Player winner = g.RollOffCompetition(g.Players);
			Assert.Equal(p4.Object,winner);

		}

		*//* Only checks upper bound currently. Lower bound needs checking by seeing if game starts when adding second player (that func. doesn't exist yet AFAIK *//*
		[Fact]
		public void PlayerLimitTest()
		{
			GameController g = new GameController();	//Alec initialized in Const.

			Player p = new Player("Austin J", new DateTime(0000, 00, 02));
			g.AddPlayer(p);

			p = new Player("Austin J", new DateTime(0000, 00, 03));
			g.AddPlayer(p);

			p = new Player("Austin J", new DateTime(0000, 00, 04)); 
			g.AddPlayer(p);

			p = new Player("Austin J", new DateTime(0000, 00, 05));
			g.AddPlayer(p);

			p = new Player("Austin J", new DateTime(0000, 00, 06));
			g.AddPlayer(p);

			Assert.Equal(6, g.PlayerCount);
			
			p = new Player("Austin J", new DateTime(0000, 00, 07));
			g.AddPlayer(p);
			
			Assert.Equal(6, g.PlayerCount);
		}
		*//*
		 *TODO: REWRITE TESTS......
		[Fact]
		public void SingletonTest()
		{
			GameController g = GameController.Instance;
			GameController g2 = GameController.Instance;
			Assert.Equal(g, g2);
		}

		[Fact]
		public void AddingPlayerTest()
		{
			GameController g = GameController.Instance;
			Player p = new Player("Alec H", new DateTime(1997, 11, 16));


			g.AddPlayer(p);
			Assert.Equal(g.Players[0], p);

			Assert.Single(g.Players);
		}

		[Fact]
		public void RemovingPlayerTest()
		{
			GameController g = GameController.Instance;
			List<Player> players = new List<Player>(g.Players);
			foreach (var p in players)
			{
				g.RemovePlayer(p);
			}

			Assert.Empty(g.Players);
		}

		[Fact]
		public void PlayerLimitTest()
		{

		}

		[Fact]
		public void DeterminePlayerOrderTest()
		{
			GameController g = GameController.Instance;
			Mock<Player> p1 = new Mock<Player>("Alec H", new DateTime(1997, 1, 16));
			Mock<Player> p2 = new Mock<Player>("Chris P", new DateTime(1989, 5, 5));
			Mock<Player> p3 = new Mock<Player>("Mike H", new DateTime(1921, 9, 9));
			Mock<Player> p4 = new Mock<Player>("Ben D", new DateTime(1999, 12, 21));

			p1.Setup(f => f.RollDice(1)).Returns(new int[1] { 1 });
			p2.Setup(f => f.RollDice(1)).Returns(new int[1] { 2 });
			p3.Setup(f => f.RollDice(1)).Returns(new int[1] { 3 });
			p4.Setup(f => f.RollDice(1)).Returns(new int[1] { 4 });

			Assert.Equal(new int[1] { 1 }, p1.Object.RollDice(1));
			Assert.Equal(new int[1] { 2 }, p2.Object.RollDice(1));
			Assert.Equal(new int[1] { 3 }, p3.Object.RollDice(1));
			Assert.Equal(new int[1] { 4 }, p4.Object.RollDice(1));

			g.AddPlayer(p1.Object);
			g.AddPlayer(p2.Object);
			g.AddPlayer(p3.Object);
			g.AddPlayer(p4.Object);

			List<Player> order = g.DeterminePlayerOrder();

			Assert.Equal(p4.Object, order[3]);
			Assert.Equal(p3.Object, order[2]);
			Assert.Equal(p2.Object, order[1]);
			Assert.Equal(p1.Object, order[0]);

		}
		*/
	}
}
