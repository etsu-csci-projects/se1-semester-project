using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using ScrumGame.Models;
using System.Collections;

namespace XUnitScrumGameTests
{
	public class MainFlowTileTest
	{
		[Fact]
		public void MainFlowTileConstructorTest()
		{
			MainFlowTile mf = new MainFlowTile("Requirements");
			Assert.Equal("Requirements", mf.Name);
			Assert.Equal(-1, mf.MeepleLimit);
			Assert.Null(mf.RequiredMeeple);

			List<Meeple> reqMeeples = new List<Meeple>() { Meeple.DEVELOPER, Meeple.PRODUCT_OWNER };	
			mf = new MainFlowTile("Refine", -1, reqMeeples);
			Assert.Equal("Refine", mf.Name);
			Assert.Equal(-1, mf.MeepleLimit);
			Assert.Equal(reqMeeples, mf.RequiredMeeple);
		}

		[Fact]
		public void AddPlayerRequirementRoundTest()
		{
			MainFlowTile mf = new MainFlowTile("Requirements");
			Player p = new Player("Alec", new DateTime(2020, 11, 16));
			int roll = p.RollDice(1)[0];
			int result = mf.AddPlayerRequiredRound(p, roll);

			Assert.Equal(roll, result);

			Dictionary<Player, int> test = new Dictionary<Player, int>() { { p, roll } };

			Assert.Equal(test , mf.PlayersRequiredRounds);
		}

		[Fact]
		public void GetRoundRequiredTest()
		{ 
			MainFlowTile mf = new MainFlowTile("Requirements");
			Player p = new Player("Alec", new DateTime(2020, 11, 16));
			int roundsReq = mf.GetRoundsRequired(p);
			Assert.Equal(-1, roundsReq);

			int roll = p.RollDice(1)[0];
			mf.AddPlayerRequiredRound(p, roll);
			roundsReq = mf.GetRoundsRequired(p);

			Assert.Equal(roll, roundsReq);
		}

		[Fact]
		public void UpdateRoundsRequiredTest()
		{ 
			MainFlowTile mf = new MainFlowTile("Requirements");
			Player p = new Player("Alec", new DateTime(2020, 11, 16));
			int roll = p.RollDice(1)[0];
			mf.AddPlayerRequiredRound(p, roll);
			int result = mf.GetRoundsRequired(p);
			Assert.Equal(roll, result);

			mf.UpdateRoundsRequired(p, result - 1);
			result = mf.GetRoundsRequired(p);
			Assert.Equal(roll - 1, result);
		}

		[Fact]
		public void RemovePlayerRequirementTest()
		{ 
			MainFlowTile mf = new MainFlowTile("Requirements");
			Player p = new Player("Alec", new DateTime(2020, 11, 16));
			int roll = p.RollDice(1)[0];
			mf.AddPlayerRequiredRound(p, roll);

			mf.RemovePlayerRoundRequirement(p);
			Assert.Empty(mf.PlayersRequiredRounds);
		}


	}
}
