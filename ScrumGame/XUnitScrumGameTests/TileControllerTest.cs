﻿using System;
using System.Collections.Generic;
using System.Text;
using ScrumGame.Controllers;
using ScrumGame.Models;
using Xunit;

namespace XUnitScrumGameTests
{
	public class TileControllerTest
	{
		TileController tileController = new TileController();
		Player player = new Player("Alec H", new DateTime(2020, 11, 16));
		MainFlowTile mfTile = new MainFlowTile("Requirements");

		[Fact]
		public void DetermineRoundsSpendTest()
		{
			int rounds = tileController.DetermineRoundsSpent(player, mfTile);
			Assert.InRange(rounds, 1, 6);
			Assert.InRange(mfTile.GetRoundsRequired(player), 1, 6);
			int assigned = mfTile.GetRoundsRequired(player);
			Assert.Equal(rounds, assigned);
		}

		[Fact]
		public void GetRemainingRoundsTest()
		{
			int rounds = tileController.DetermineRoundsSpent(player, mfTile);
			int result = tileController.GetRemainingRounds(player, mfTile);
			Assert.InRange(result, 1, 6);
			Assert.Equal(rounds, result);
		}

		[Fact]
		public void UpdateRemainingRoundTest()
		{ 
			int rounds = tileController.DetermineRoundsSpent(player, mfTile);
			tileController.UpdateRemainingRound(player, mfTile, 15);
			Assert.NotEqual(rounds, tileController.GetRemainingRounds(player, mfTile));
			Assert.Equal(15, tileController.GetRemainingRounds(player, mfTile));
		}

		[Fact]
		public void AddMeepleToRequirementsTileTest()
		{
			List<Meeple> meeplesToAdd = new List<Meeple> { Meeple.PRODUCT_OWNER, Meeple.SCRUM_MASTER };
			tileController.AddMeepleToMainFlowTile(player, mfTile, meeplesToAdd);
			Assert.Equal(4, player.Meeples.Count);

			Dictionary<Player, List<Meeple>> dict = new Dictionary<Player, List<Meeple>>();
			dict.Add(player, meeplesToAdd);

			Assert.Equal(dict, mfTile.PlayerAssignedMeeples);
		}
	}
}
