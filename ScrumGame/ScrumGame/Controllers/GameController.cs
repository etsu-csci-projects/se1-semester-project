﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ScrumGame.Models;
using ScrumGame.Services;

namespace ScrumGame.Controllers
{
	/// <summary>
	/// Singleton pattern implemented from ("Second version - simple thread-safety"):
	///	https://csharpindepth.com/Articles/Singleton
	/// </summary>	
	public class GameController : Controller
    {
		//private static GameControllerSingleton instance = null;

		//private static readonly object padlock = new object();

		public int MinPlayerCount { get { return 2; } }

		public int MaxPlayerCount { get { return 6; } }

		public int PlayerCount { get { return this.Players.Count; } }

		public List<Player> Players { get; private set; }

		public Player Winner { get; private set; }

		private readonly IPlayerRepository _playerRepo;

		/// <summary>
		/// Constructs the instance of the GameControllerSingleton object
		/// </summary>
		public GameController(IPlayerRepository playerRepo)
		{
			_playerRepo = playerRepo;
			this.Players = new List<Player>() { new Player("Alec H", new DateTime(1997, 11, 19))};
			if (InitializeGame() == 1)
			{
				//no
/*				while (true)//????????????????
				{
					//Game Loop goes here
					
					// End the turn here
					GiveWorkshopTokens();
					
					//Ends the Game if a winner has been declared
					if (this.Winner != null)
						break;
				}
*/				//Do stuff for winner here
			}
			else
			{
				//throw error
			}
		}

		public async Task<IActionResult> Index()
        {
			var model = await _playerRepo.ReadAllAsync();
			return View(model);
        }

		/// <summary>
		/// Increments the number of completed user stories for player p and checks if they won.
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		public Player CompletedUserStory(Player p)
		{
			p.UseCasesCompleted++;
			return WinByUseCasesCheck(p);
		}


		/// <summary>
		/// Takes a list of Players (optional) or All the players to give a workshop token
		/// </summary>
		/// <param name="ps">List of players to give workshop tokens to</param>
		/// <returns></returns>
		public int GiveWorkshopTokens(List<Player> ps = null)
		{
			List<Player> players = ps ?? this.Players;
			foreach (var p in players)
				p.WorkshopTokens++;

			return 1;
		}

		/// <summary>
		/// Initalizes the setup of the game...
		/// </summary>
		/// <returns>integer representing if the game was successfully setup or not</returns>
		public int InitializeGame()
		{
			return 1;
		}

		/// <summary>
		/// Checks if a Player has met the WinByUseCases condition. If more than 1 at the same time, throws an error.
		/// Do not wait till end of round to call this or we will encounter errors.
		/// </summary>
		/// <returns></returns>
		public Player WinByUseCasesCheck(Player player=null)
        {
			Player winner = null;
			//If Player has been given, only check that player
			if (player != null) 
			{
				if (player.UseCasesRemaining == 0)
					winner = player;
				return winner;
			}

			foreach (var p in this.Players )
			{
				if (p.UseCasesRemaining == 0)
				{
					if (winner != null)
					{
						//Throw some kind of error - this should never be triggered though
					}
					winner = p;
				}
			}
			return winner;
        }
		/// <summary>
		/// Part of the singleton design patern, used to aquire the instance of the GameController
		/// </summary>
		/*
		public static GameControllerSingleton Instance
		{
			get
			{
				lock (padlock)
				{
					if (instance == null)
					{
						instance = new GameControllerSingleton();
					}
					return instance;
				}
			}
		}
		*/

		/// <summary>
		/// Commented this and the AddPlayer methods out temporarily - Subject to removal. - Adam
		/// </summary>
		/// <returns></returns>
		// public IActionResult PlayersList()
        // {
		// 	var model = _playerRepo.ReadAll();
        //     return View(model);
        // }

		//public IActionResult AddPlayer()
  //      {
		//	return View();
  //      }

		//[HttpPost]
		//public IActionResult AddPlayer(Player p)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		if (Players.Count < this.MaxPlayerCount)
		//		{
		//			p.Money = GetAverageMoney();
		//			_playerRepo.Create(p);
		//			return RedirectToAction("PlayersList");
		//		}
		//		else
		//			return View(p);
		//	}
		//	else
  //          {
		//		return View(p);
		//	}
		//}

		public int GetAverageMoney(int starterMoney=500) 
		{
			int sum = 0, count;

			for (count = 0; count < Players.Count; count++)
				sum += Players[count].Money;

			return ( count > 0 ) ? ( sum / count ) : starterMoney;	//Returns average if there is more than 0 players.
		}

		/// <summary>
		/// Removes a player
		/// </summary>
		/// <param name="p">The player to remove</param>
		/// <returns>Integer representing if the game can still continue after removing the player</returns>
		public int RemovePlayer(Player p)
		{
			if (Players.Count > this.MinPlayerCount)
			{
				Players.Remove(p);
				return 0;
			}
			else
			{
				Players.Remove(p);
				return -1;
			}
				
		}


		public List<Player> DeterminePlayerOrder()
		{
			int[] OrderIndex = new int[this.PlayerCount];
			Player[] PlayerOrder = new Player[this.PlayerCount];

			/* fills with defaults */
			for (int i = 0; i < this.PlayerCount; i++)
				OrderIndex[i] = i;

			/* find index of winner */
			Player winner = RollOffCompetition(this.Players);
			int WinnerNum = Array.IndexOf(this.Players.ToArray(), winner);
			
			/* loops through remaining indices */
			OrderIndex[0] = WinnerNum;
			int CurrentLoserNum = (WinnerNum < this.PlayerCount-1) ? WinnerNum + 1: 0;
			for (int i = 1; i < this.PlayerCount; i++)
			{
				if (CurrentLoserNum == this.PlayerCount - 1)
					CurrentLoserNum = 0;
				else
					CurrentLoserNum++;

				OrderIndex[i] = CurrentLoserNum;
			}

			/* stores each player based on index */
			foreach (var index in OrderIndex)
				PlayerOrder[index] = this.Players[index];

			return PlayerOrder.ToList();
		}
		
		//TODO: I NEED TO TEST THIS METHOD 
		//PROBABLY NOT WORKING YET DO NOT TRUST THIS
		/*public List<Player> DeterminePlayerOrder()
		{
			Dictionary<Player, int> playerRolls = new Dictionary<Player, int>();

			int maxRoll = int.MinValue;
			foreach (var p in Players)
			{
				int pRoll = p.RollDice(1)[0];
				if (pRoll > maxRoll)
					maxRoll = pRoll;
				playerRolls.Add(p, pRoll);
			}
			
			//this could be removed i guess?? since we only really care about who is going first
			var playerOrder = from entry in playerRolls orderby entry.Value ascending select entry;

			List<Player> firstCompetition = new List<Player>();
			foreach (var p in playerOrder)
			{
				int pRoll = p.Value;
				if (maxRoll == pRoll)
					firstCompetition.Add(p.Key);
			}


			if (firstCompetition.Count > 1)
			{
				//go do a roll off
			}


			this.Players.Clear();

			foreach (var p in playerOrder)
			{
				this.Players.Add(p.Key);
			}

			return this.Players;
		}*/

		/*struct PlayerCompetitionEntry
		{
			public Player player;
			public bool disqualified;
		}*/

		//TODO: I NEED TO TEST THIS METHOD
		//PROBABLY NOT WORKING YET DO NOT TRUST THIS
		public Player RollOffCompetition(List<Player> players=null)
		{
			/*PlayerCompetitionEntry[] entries = new PlayerCompetitionEntry[players.Count];
			for (int i = 0; i < players.Count; i++)
			{
				PlayerCompetitionEntry entry;
				entry.player = players[i];
				entry.disqualified = false;
				entries[i] = entry;
			}*/
			// Takes an optional parameter (or uses all playeres) and makes a deep copy of it using LINQ. 
			/* https://stackoverflow.com/questions/14007405/how-create-a-new-deep-copy-clone-of-a-listt */
			List<Player> RollingPlayers = ( players ?? this.Players )
						.Select(player => new Player(player.Name, Convert.ToDateTime(player.DoB)))
						.ToList();
					
			int difference = 0;
			int maxRoll = int.MinValue;
			int currentRoll = int.MinValue;
			while (RollingPlayers.Count > 1)
			{
				for (int i = 0; i < RollingPlayers.Count;)
				{
					currentRoll = RollingPlayers[i].RollDice(1)[0];

					if (currentRoll > maxRoll)
						maxRoll = currentRoll;

					difference = currentRoll - maxRoll;

					if (difference != 0)
						RollingPlayers.RemoveAt(i);
					else
						i++;
				}
			}
			/*while (difference == 0)
			{
				for(int i = 0; i < entries.Length; i++)
				{
					if (!entries[i].disqualified)
					{
						currentRoll = entries[i].player.RollDice(1)[0];

						if (currentRoll > maxRoll)
							maxRoll = currentRoll;

						difference = currentRoll - maxRoll;

						if (difference != 0)
							entries[i].disqualified = true;
					}
				}
			}*/

			//return (from p in entries where !p.disqualified select p.player).First();
			return RollingPlayers[0];
		}
    }
}
