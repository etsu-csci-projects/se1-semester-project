﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ScrumGame.Models;
using ScrumGame.Services;

namespace ScrumGame.Controllers
{
    public class PlayersController : Controller
    {
        private readonly IPlayerRepository _playerRepo;

        public PlayersController(IPlayerRepository playerRepo)
        {
            _playerRepo = playerRepo;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Player player)
        {
            var newPlayer = await _playerRepo.CreateAsync(player);
            //return Json(new { newPlayer.Id });
            
            return RedirectToAction("Index", "Game");
        }

        public async Task<IActionResult> PlayerRow(int id)
        {
            var player = await _playerRepo.ReadAsync(id);
            return PartialView("/Views/Home/_PlayerRow.cshtml", player);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            await _playerRepo.DeleteAsync(id);
            //return Json(new { id });
            return RedirectToAction("Index", "Game");

        }
    }
}
