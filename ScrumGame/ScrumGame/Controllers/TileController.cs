using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ScrumGame.Models;

namespace ScrumGame.Controllers
{
	/// <summary>
	/// Singleton pattern implemented from ("Second version - simple thread-safety"):
	///	https://csharpindepth.com/Articles/Singleton
	/// </summary>	
	public class TileController : Controller
	{
		private MainFlowTile RequirementsTile { get; set; }
		public TileController()
		{
			List<Meeple> requiredReqsTile = new List<Meeple>() { Meeple.SCRUM_MASTER, Meeple.PRODUCT_OWNER };
			this.RequirementsTile = new MainFlowTile("Requirements Gathering", -1, requiredReqsTile);
		}

		public IActionResult Index()
		{
			return View();
		}


		/// <summary>
		/// Determines the rounds spent.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <param name="mfTile">The mainflow tile.</param>
		/// <returns></returns>
		public int DetermineRoundsSpent(Player player, MainFlowTile mfTile)
		{
			int roundsRequired = player.RollDice(1)[0];
			return mfTile.AddPlayerRequiredRound(player, roundsRequired);
		}

		public int GetRemainingRounds(Player player, MainFlowTile mfTile)
		{
			return mfTile.GetRoundsRequired(player);
		}

		public bool UpdateRemainingRound(Player player, MainFlowTile mfTile, int remainingRounds)
		{
			return mfTile.UpdateRoundsRequired(player, remainingRounds);
		}

		public void AddMeepleToMainFlowTile(Player player, MainFlowTile mfTile, List<Meeple> meeples)
		{
			mfTile.AssignPlayersMeeple(player, meeples);
		}
	}
}
