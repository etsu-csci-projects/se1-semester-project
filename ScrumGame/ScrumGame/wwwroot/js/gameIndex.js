﻿'use strict';
$(document).ready(function _homeIndex() {

    $('#createPlayerForm').on('submit', function _submitPlayerForm(event) {
        event.preventDefault();
        let $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: $form.serialize(),
            success: function _ajaxSuccess(response) {
                _updatePlayersTable(response.id);
                $('#createPlayerModal').modal('hide');
            },
            error: function _error() {

            }
        });
    });

    function _updatePlayersTable(id) {
        $.ajax({
            url: '/players/playerrow/' + id,
            success: function _success(response) {
                $('#tbody-players').append(response);
                $('#messageArea').html("A new player was added!");
                $('#alertArea').show(400)

            },
            error: function _error() {
                console.log("There was an error inserting the player row.")
            }
        });
    }

    $('#alertCloseBtn').on('click', function _hideAlert() {
        $('#alertArea').hide(400);
    });

    $('.removeBtn').on('click', function _removePlayer(event) {
        event.preventDefault();
        let $link = $(this);
        let theUrl = $link.attr('href');
        let tokens = theUrl.split('/');
        let id = tokens[tokens.length - 1];
        $.ajax({
            url: theUrl,
            type: "post",
            data: { id: id },
            success: function _success(response) {
                _updatePlayersTable(response.id);
            },
            error: function _error() {
                $('#messageArea').html("An unknown error occurred!");
                $('#alertArea').show(400);
            }
        });
    });

});