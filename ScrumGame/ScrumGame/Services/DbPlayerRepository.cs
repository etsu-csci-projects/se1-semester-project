﻿using Microsoft.EntityFrameworkCore;
using ScrumGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Services
{
	public class DbPlayerRepository : IPlayerRepository
	{
		private readonly PlayerDbContext _db;
		public DbPlayerRepository(PlayerDbContext db)
		{
			_db = db;
		}
		public async Task<Player> CreateAsync(Player player)
		{
            await _db.Players.AddAsync(player);
			await _db.SaveChangesAsync();
			return player;
		}

		public async Task<Player> ReadAsync(int id)
        {
			return await _db.Players.FirstOrDefaultAsync(p => p.Id == id);
        }

		public async Task<ICollection<Player>> ReadAllAsync()
        {
			return await _db.Players.ToListAsync();
        }

		// NOTE: Update method may not be needed, given the limited 
		//       information required of players to be given. I am not sure of this. - Adam
		//public void Update(int oldId, Player player)
		//{
		//	throw new NotImplementedException();
		//}

		public async Task DeleteAsync(int id)
		{
			Player playerToDelete = await ReadAsync(id);
			_db.Players.Remove(playerToDelete);
			_db.SaveChanges();
		}
	}
}
