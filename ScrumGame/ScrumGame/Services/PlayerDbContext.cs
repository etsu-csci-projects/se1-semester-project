﻿using Microsoft.EntityFrameworkCore;
using ScrumGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Services
{
	public class PlayerDbContext : DbContext
	{
		public PlayerDbContext(DbContextOptions options) : base(options)
		{
		}

		public DbSet<Player> Players { get; set; }
	}
}
