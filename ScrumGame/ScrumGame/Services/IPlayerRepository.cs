﻿using ScrumGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Services
{
	public interface IPlayerRepository
	{

		Task<Player> CreateAsync(Player player);

		Task<Player> ReadAsync(int id);

		Task<ICollection<Player>> ReadAllAsync();

		//void Update(int oldId, Player player);

		Task DeleteAsync(int playerId);
	}
}
