using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Models
{
  /// <summary>
  /// Static class designed to store remaining Cards.
  /// Author: Jones, Austin
  /// </summary>
  public static class Deck
  {
    /* List of User Stories */
    public static List<string> userStories = new List<string> {
      "t",
      "e",
      "s",
      "t"
    };

    /* List of Unique Goal Cards for player's objective */
    public static List<string> productGoals = new List<string> {
      "p",
      "r",
      "o",
      "d",
      "u",
      "c",
      "t"
    };

    /* combined data so that type may be used as an index instead of having two branches for type */
    public static List<String>[] cardsInDeck = { userStories, productGoals };

    /// <summary>
    /// pulls a random string of type type from the remaining cards in deck.
    /// </summary>
    /// <param name="type">Type of Card based on CardType enum</param>
    /// <returns></returns>
    public static string pullMessage(CardType type, int index = -1)
    {
      string message;
      int i = getCardIndex(type, index);
      
      message = cardsInDeck[( int ) type ][i];
      cardsInDeck[( int ) type ].RemoveAt ( i );
      return message;
    }
   /// <summary>
   /// Wrapper for getting Card Index
   /// </summary>
   /// <param name="type"> Deck to choose from </param>
   /// <param name="NegativeOneForRandom"> Use negative one for random or anything else for that value </param>
   /// <returns>Card Index</returns>
    public static int getCardIndex(CardType type, int NegativeOneForRandom) 
    {
        Random r = new Random();
        return (NegativeOneForRandom == -1) ?
            r.Next(cardsInDeck[(int)type].Count) :
            NegativeOneForRandom;
    }
  }
}
