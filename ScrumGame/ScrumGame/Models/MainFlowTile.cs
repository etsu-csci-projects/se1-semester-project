﻿using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Models
{
	public class MainFlowTile : Tile
	{

		public Dictionary<Player, int> PlayersRequiredRounds { get; private set; }

		public Dictionary<Player, List<Meeple>> PlayerAssignedMeeples { get; private set; }
		

		/// <summary>
		/// Initializes a new instance of the <see cref="MainFlowTile"/> class.
		/// [required meeple parameter type WILL PROBABLY change in the future]
		/// [<see cref="Tile"/> class.]
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="meepleLimit">The meeple limit.</param>
		/// <param name="requiredMeeple">The required meeple.</param>
		public MainFlowTile(String name, int meepleLimit = -1, List<Meeple> requiredMeeple = null)
		{
			this.Name = name;
			this.MeepleLimit = meepleLimit;
			this.RequiredMeeple = requiredMeeple;
			PlayersRequiredRounds = new Dictionary<Player, int>();
			PlayerAssignedMeeples = new Dictionary<Player, List<Meeple>>();
		}

		/// <summary>
		/// Adds a player's round requirement to the class dictionary
		/// </summary>
		/// <param name="player">The player.</param>
		/// <returns>The number of rounds required for the player</returns>
		public int AddPlayerRequiredRound(Player player, int roundsRequired)
		{
			if (!this.PlayersRequiredRounds.ContainsKey(player))
			{
				this.PlayersRequiredRounds.Add(player, roundsRequired);
				return roundsRequired;
			}
			else
				return this.PlayersRequiredRounds[player];
		}

		/// <summary>
		/// Gets the rounds required for a specific player
		/// </summary>
		/// <param name="player">The player.</param>
		/// <returns>The number of rounds required for a given player. -1 if player could not be found</returns>
		public int GetRoundsRequired(Player player)
		{
			if (this.PlayersRequiredRounds.ContainsKey(player))
				return this.PlayersRequiredRounds[player];
			else
				return -1;
		}

		/// <summary>
		/// Updates the rounds required.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <param name="remainingRounds">The remaining rounds.</param>
		/// <returns>true if the update was successful and false if the update failed</returns>
		public bool UpdateRoundsRequired(Player player, int remainingRounds)
		{
			if (this.PlayersRequiredRounds.ContainsKey(player))
			{
				this.PlayersRequiredRounds[player] = remainingRounds;
				return true;
			}
			else
				return false;
		}


		/// <summary>
		/// Removes the player round requirement.
		/// </summary>
		/// <param name="player">The player.</param>
		/// <returns>true if remove successful, false if remove failed</returns>
		public bool RemovePlayerRoundRequirement(Player player)
		{
			if (PlayersRequiredRounds.ContainsKey(player))
			{ 
				this.PlayersRequiredRounds.Remove(player);
				return true;
			}
			else
				return false;
		}

		/// <summary>
		/// Assigns a particular player's meeple to a given tile
		/// </summary>
		/// <param name="player"></param>
		/// <param name="meeples"></param>
		public void AssignPlayersMeeple(Player player, List<Meeple> meeples)
		{
			this.PlayerAssignedMeeples.Add(player, meeples);
			foreach (Meeple m in meeples)
			{
				player.RemoveMeeple(m);
			}

		}



	}
}
