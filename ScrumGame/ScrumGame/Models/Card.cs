using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Models
{
  /// <summary>
  /// Card Type enumeration
  /// </summary>
  public enum CardType
  {
      UserStory = 0,
      ProductGoal = 1
  }
  /// <summary>
  /// Data representation of a Card, be that User Stories, Product Goal Cards, etc.
  /// </summary>
  [NotMapped]
  public class Card 
  {  
    /* String containing the message of the Card */
    public string message { get; }

    /* Contains type of card for easier enumeration in Controllers */
    public CardType type { get; }
    
    /// <summary>
    /// Generates a card based on type and a message string
    /// </summary>
    /// <param name="c">Card Type to create</param>
    public Card(CardType c, int index=-1) 
    {
      message = Deck.pullMessage( type = c, index );
    }

  }
}
