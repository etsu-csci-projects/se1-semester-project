using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumGame.Models
{
    /* classes of meeples */
    public enum Meeple
    {
        DEVELOPER = 2,    //Updated to fit into Alec's model labels.
        SCRUM_MASTER = 1,
        PRODUCT_OWNER = 0,
        DUMMY = -1        //Needed to replace null qualifiers
    }
}
 
