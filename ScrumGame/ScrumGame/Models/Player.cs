﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ScrumGame.Models
{
	public class Player
	{
		private const int _INIT_NUMBER_DEVS = 4;
		private const int _USE_CASES_TO_WIN = 5;

		private Random Rand = new Random();
		public List<Meeple> Meeples = new List<Meeple>();
		public int WorkshopTokens { get; set; }
		public int Money { get; set; }
		private List<Card> UseCases = new List<Card>();
		public int UseCasesCompleted { get; set; }
		public int UseCasesRemaining { get { return _USE_CASES_TO_WIN - UseCasesCompleted; } }
		public Card Goal { get; private set; }

		[Required]
		public String Name { get; set; }

		[Required]
		public String DoB { get; set; } 

		public int Score { get; set; }
		public int Id { get; set; }

		public Player()
		{
			this.Name = "Default";
			this.DoB = DateTime.Now.ToShortDateString();
			this.UseCasesCompleted = 0;
		}
		public Player(string Name, DateTime DoB)
		{
			this.Name = Name;
			this.DoB = DoB.ToShortDateString();
			this.UseCasesCompleted = 0;
			InitMeeple();
		}

		public virtual int[] RollDice(int amount)
		{
			int[] result = new int[amount];
			for (int i = 0; i < amount; i++)
			{
				result[i] = Rand.Next(1, 6);
			}
			return result;
		}
		/* Gives the player one of the remaining goal cards */
		public Card GetGoal(int index = -1)	//DON'T USE INDEX- for unit testing only
		{
			/* Errors if user already has a Goal */
			if (Goal != null) {
				//error now
				return null;
			}
			Goal = new Card( CardType.ProductGoal, index );
			return Goal;
		}

		/// <summary>
		/// Initializes the beginning set of Meeples and gives them to the current player
		/// </summary>
		/// <returns>0 | 1 for failure or success respectively</returns>
		private int InitMeeple () 
		{
			int success = 0;
			Meeples.Add(Meeple.SCRUM_MASTER );
			Meeples.Add(Meeple.PRODUCT_OWNER );
			for (int i = 0; i < _INIT_NUMBER_DEVS ; i++)
				Meeples.Add(Meeple.DEVELOPER );
			success = 1;
			return success;
		}

		public void AddMeeple(Meeple meeple)
		{
			if (this.Meeples.Count < _INIT_NUMBER_DEVS + 4) //Changed to allow upto 2 additional devs to be hired
			{
				this.Meeples.Add(meeple);
			}
		}

		public void RemoveMeeple(Meeple meeple)
		{
			Meeple toRemove = Meeple.DUMMY;
			foreach (Meeple m in this.Meeples)
			{
				if (m == meeple)
				{ 
					toRemove = m;
					break;
				}
			}
			if(toRemove != Meeple.DUMMY)
				this.Meeples.Remove(toRemove);
		}

	}
}
