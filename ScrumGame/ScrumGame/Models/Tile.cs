﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ScrumGame.Models
{
	/// <summary>
	/// https://www.tutorialspoint.com/Optional-property-in-a-Chash-class
	/// </summary>
	/// <seealso cref="System.Attribute" />
	[AttributeUsage(AttributeTargets.Property,
		Inherited = false,
		AllowMultiple = false)]
	internal sealed class OptionalAttribute : Attribute { }

	public abstract class Tile
	{
		public String Name { get; set; }

		public int MeepleLimit { get; set; }

		/// <summary>
		/// Gets the required meeple.
		/// </summary>
		/// <value>
		/// The required meeple.
		/// </value>
		[Optional]
		public List<Meeple> RequiredMeeple { get; set; }
	}
}
